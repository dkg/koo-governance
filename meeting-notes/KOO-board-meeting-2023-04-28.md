Present: Daniel, Vincent, Ola, Lukas, Neal
Absent: 
Absent with excuses:

# Agenda

* Discuss action items from previous meeting and their progress
* Operations team report
* Continue discussion on whether or not KOO should position itself as CA or not 


# Action Items from previous meeting

- [ ] Lukas: look into available CoC's and talk to people who might have more experience
- [ ] Neal: Invite Expert to talk about GDPR and keyservers. Either next regular meeting or based on offer by Expert (on UK time)
- [ ] Vincent: document simple process for joining the election body
- [ ] Vincent: send out voting body invites
- [ ] Neal: send invites to the individual members we would like to add to the voting body
- [x] Write introductory item about the Board on KOO
  - At the openpgp email summit decided to move to a community-based governance model
  - First election was in Nov
  - New board consisting of xxx first convened in Dec
  - Looking for people to join the election body (link to Vincent's post)
  - The server is running nicely without major issues
  
  - Vincent on tone:
    - wants the blog to be approachable to end users (not technical; no talk about specs)
    - Emoji or two at the end, but not all over
- [ ] Vincent + Lukas: discuss and document the single components of the current server infrastructure and tasks the operations team is – and future members would be – responsible for.
- [x] Daniel: Write an abstract version of the rate limiting process – what information to include, point of contact – based on the rate limiting request from Daniel for Proton
- [x] Vincent: increase rate limit as per proton's request


# Operations team report

* Increase of rate limit for Proton IPs
* Vincent provided some KOO statistics to a researcher and their paper was now published:
    Authenticated private information retrieval
    https://www.usenix.org/system/files/sec23fall-prepub-78-colombo.pdf
* Short outage of KOO for about half an hour where one of the virtual machines died. Working again after rebuild.
* No fallback at the moment. Discuss redundancy in the future.


# Continue discussion on whether or not KOO should be a CA

* Wait for feedback to email sent out by Vincent to the voting body
* Bring this discussion up at the OpenPGP Email Summit since many people from the voting body might be there.


# Action Items to discuss in next meeting: May 12th, 2023
- [ ] Lukas: look into available CoC's and talk to people who might have more experience
- [ ] Neal: Invite Expert to talk about GDPR and keyservers. Either next regular meeting or based on offer by Expert (on UK time). To make it easier to find a date, only a sub group will meet if the date doesn't work for everyone.
- [ ] Add a sentence how to join a voting body to the news item introducing the board
- [ ] Vincent + Lukas: discuss and document the single components of the current server infrastructure and tasks the operations team is – and future members would be – responsible for.
  -> Vincent will ask again, who was interested in doing it before. If they don't have time soon, Lukas and Vincent will find some time in May.
- [ ] Vincent: lookup stats how many requests / second/hour KOO receives approximately to determine if a fallback might make sense. How many read operations / how many write operations
- [ ] Vincent: send an email to the voting body mentioning that this should be discussed at the OpenPGP Email Summit


