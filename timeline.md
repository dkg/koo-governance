# Timeline for KOO Organization Bootstrapping

- 4 weeks public comment period after publication of draft organizational documents.
- 1 week for bootstrap committee to complete incorporation of public comments and publish the first formal version of the constitution.
- 4 weeks of open invitation for the first round of the voting body
- 2 weeks for the election of the first board
- 3 days to confirm acceptance of most-approved candidates
