# Rate Limit Increase Request Process

To request a rate limit increase for the [keys.openpgp.org API](https://keys.openpgp.org/about/api), please send an email to [board@keys.openpgp.org](mailto:board@keys.openpgp.org).

The email should contain the following information:

- The IP address (range) from which you will make requests to the API
- The desired maximum request rate for each endpoint you want to use
- The expected typical average and burst request rate for each endpoint you want to use
- The intended purpose of the requests
- The storage policy (e.g., how long responses will be stored, if at all)
- A point of contact in case of issues

Then, the board will discuss the request at the next meeting and get back to you with a decision or further questions if necessary.
