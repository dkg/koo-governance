# `keys.openpgp.org` Governance

[`keys.openpgp.org` (a.k.a. KOO)](https://keys.openpgp.org/) is a network service operated on behalf of the OpenPGP community to share OpenPGP certificates to facilitate cryptographically-protected e-mail.

The KOO service is the primary user of [the Hagrid keyserver software](https://gitlab.com/keys.openpgp.org/hagrid).

This repository documents the governance of the KOO service.

In 2022, a Bootstrap Committee was formed at the [OpenPGP E-mail Summit](https://wiki.gnupg.org/OpenPGPEmailSummit202205), with a goal of establishing a governance mechanism for the network service. The Bootstrap Committee organized a vote for the initial Board of KOO, which was concluded on November 9. The initial Board assumed its duties on December 1, 2022.

The current Board assumed its duties on December 1, 2023.
The members of the Board are:

- Vincent Breitmoser (@valodim)
- Daniel Huigens (@twisstle)
- Ola Bini (@olabiniV2)
- Lukas Pitschl (@luke_le)

The Board serves for a one-year term, until December 1, 2024.

This repository contains the [meeting notes](./meeting-notes/) of the Board meetings, and they are also posted to the [koo-voting mailing list](https://lists.hostpoint.ch/archives/list/koo-voting@enigmail.net/latest).
